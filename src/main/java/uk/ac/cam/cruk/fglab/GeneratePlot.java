package uk.ac.cam.cruk.fglab;

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.PlugIn;
import ij.plugin.frame.RoiManager;
import ij.gui.Plot;
import ij.process.ImageStatistics;
import ij.gui.PlotWindow;
import ij.gui.Roi;
import ij.measure.ResultsTable;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.awt.Color;

import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.stat.descriptive.moment.Mean;

public class GeneratePlot implements PlugIn {

	
	

public static void plotWithImgAndRoiManager(
		ImagePlus imp
		) {
	//imp = IJ.getImage();
	//impOri.deleteRoi();
	//ImagePlus imp = impOri.duplicate();
	RoiManager rm = RoiManager.getInstance();
	if (rm == null) return;
	Roi[] roiArray = ROIUtility.managerToRoiArray();
	
	int numSpot = rm.getCount();

	
	double[] t = new double[numSpot];
	double[] c1 = new double[numSpot];
	double[] c2 = new double[numSpot];
	double[] c3 = new double[numSpot];
	/*
	 * Mitosis plot inactive!!!
	 */
	//ArrayList<Double> mitosisIdx = new ArrayList<Double>();
	//double maxValue = 0;
	
	for (int i=0; i<roiArray.length; i++) {
		imp.setPositionWithoutUpdate(
				roiArray[i].getCPosition(), 
				roiArray[i].getZPosition(), 
				roiArray[i].getTPosition());
		imp.setRoi(roiArray[i], false);
		t[i] = (double)imp.getFrame();

		imp.setPositionWithoutUpdate( 1, 
				roiArray[i].getZPosition(), 
				roiArray[i].getTPosition());
		ImageStatistics stats = imp.getStatistics();
		c1[i] = stats.mean;
		imp.setPositionWithoutUpdate( 2, 
				roiArray[i].getZPosition(), 
				roiArray[i].getTPosition());
		stats = imp.getStatistics();
		c2[i] = stats.mean;
		imp.setPositionWithoutUpdate( 3, 
				roiArray[i].getZPosition(), 
				roiArray[i].getTPosition());
		stats = imp.getStatistics();
		c3[i] = stats.mean;
		
		/*
		 * Mitosis plot inactive!!!
		 */
		/*
		if (roiArray[i].getName().toLowerCase().contains("m")) {
			mitosisIdx.add(t[i]);
		}
		maxValue = Math.max(c1[i], maxValue);
		maxValue = Math.max(c2[i], maxValue);
		maxValue = Math.max(c3[i], maxValue);
		*/
	}
	
	//imp.close();
	System.gc();
	//float[] x = {0.1f, 0.25f, 0.35f, 0.5f, 0.61f,0.7f,0.85f,0.89f,0.95f}; // x-coordinates
    //float[] y = {2f,5.6f,7.4f,9f,9.4f,8.7f,6.3f,4.5f,1f}; // x-coordinates

    PlotWindow.noGridLines = false; // draw grid lines
    
    Plot plot = new Plot("Fucci Fluorescent Plot","Time","Fluorescent intensity");

    plot.setColor(Color.red);
    //plot.addPoints(t,c3,PlotWindow.X);
    plot.addPoints(t,c1,Plot.LINE);
    
    plot.setColor(Color.green);
    //plot.addPoints(t,c3,PlotWindow.X);
    plot.addPoints(t,c2,Plot.LINE);
    // add label
    plot.setColor(Color.blue);
    plot.addPoints(t,c3,Plot.LINE);
    //plot.setColor(Color.red);
	//plot.addLegend("red\t1st Channel");
	//plot.setColor(Color.green);
    
    /*
	 * Mitosis plot inactive!!!
	 */
    /*
    plot.setColor(new Color(185, 13, 179, 255));
    maxValue = 1000*Math.ceil(maxValue/1000);
	for (Double m : mitosisIdx) {
		int size = (int)maxValue;
		double[] xvalues = new double[size];
		double[] yvalues = new double[size];
		for (int i=0; i<size; i+=100) {
			xvalues[i] = m;
			yvalues[i] = i;
		}
		plot.add("cross", xvalues,yvalues);
	}
	*/
    
	plot.setColor(Color.black);
	//plot.addLegend("1st Channel\t2nd Channel\t3rd Channel\tmitosis");
	plot.addLegend("1st Channel\t2nd Channel\t3rd Channel");
    plot.show();
    plot.setLimitsToFit(true);
    
}

public static void plotWithImgAndRoiManager2(
		ImagePlus imp
		) {
	//imp = IJ.getImage();
	//impOri.deleteRoi();
	//ImagePlus imp = impOri.duplicate();
	RoiManager rm = RoiManager.getInstance();
	if (rm == null) return;
	Roi[] roiArray = rm.getRoisAsArray();
	
	int numSpot = rm.getCount();

	int numC = imp.getNChannels();
	int numZ = imp.getNSlices();
	int numT = imp.getNFrames();
	boolean hideImage = numSpot>numT;
	

	ArrayList<ArrayList<Double>> c1All = new ArrayList<ArrayList<Double>>();
	ArrayList<ArrayList<Double>> c2All = new ArrayList<ArrayList<Double>>();
	ArrayList<ArrayList<Double>> c3All = new ArrayList<ArrayList<Double>>();
	
	for (int t=0; t<numT; t++) {
		c1All.add(new ArrayList<Double>());
		c2All.add(new ArrayList<Double>());
		c3All.add(new ArrayList<Double>());
	}

	//double start = System.currentTimeMillis();
	int currentSlice = imp.getCurrentSlice();
	Roi roi = imp.getRoi();
	
	//dimension = imp.getWindow().getSize();
	//int width = imp.getCanvas().getWidth();
	//int height = imp.getCanvas().getHeight();
	//double mag = imp.getCanvas().getMagnification();
	if (hideImage) imp.getWindow().setVisible(false);

	//boolean pointRoi = false;
	
		for (int i=0; i<roiArray.length; i++) {

			boolean pointRoi = roiArray[i].getTypeAsString().equals("Point");
			Roi r = pointRoi ? new Roi(roiArray[i].getXBase(), roiArray[i].getYBase(), 1, 1) : roiArray[i];
			imp.setRoi(r, false);

			
			int posT = roiArray[i].getTPosition();
			imp.setPositionWithoutUpdate(1, posT, 1);

			
			c1All.get(posT-1).add(imp.getRawStatistics().mean);

			imp.setPositionWithoutUpdate(2, posT, 1);
			//imp.setC(2);
			//stats = imp.getStatistics();
			
			c2All.get(posT-1).add(imp.getRawStatistics().mean);
			//imp.setC(3);
			imp.setPositionWithoutUpdate(3, posT, 1);
			c3All.get(posT-1).add(imp.getRawStatistics().mean);
		}

		ArrayList<Double> T = new ArrayList<Double>();
	
		ArrayList<Double> c1 = new ArrayList<Double>();
		ArrayList<Double> c2 = new ArrayList<Double>();
		ArrayList<Double> c3 = new ArrayList<Double>();
		ArrayList<Double> c1Error = new ArrayList<Double>();
		ArrayList<Double> c2Error = new ArrayList<Double>();
		ArrayList<Double> c3Error = new ArrayList<Double>();
		
		double[] temp = new double[numT];
		
		for (int t=0; t<numT; t++) {
			if (c1All.get(t).size()==0) continue;
			T.add((double) (t+1));
			
			StandardDeviation sd1 = new StandardDeviation();
			for (int i=0; i<c1All.get(t).size(); i++) {
					temp[i] = c1All.get(t).get(i);
			}
	    	c1Error.add(sd1.evaluate(temp)/Math.sqrt(c1All.get(t).size()));
	    	Mean mn1 = new Mean();
	    	c1.add(mn1.evaluate(temp, 0, c1All.get(t).size()));
	    	
	    	for (int i=0; i<c2All.get(t).size(); i++) {
	    			temp[i] = c2All.get(t).get(i);
			}
	    	StandardDeviation sd2 = new StandardDeviation();
	    	c2Error.add(sd2.evaluate(temp)/Math.sqrt(c2All.get(t).size()));
	    	Mean mn2 = new Mean();
	    	c2.add(mn2.evaluate(temp, 0, c2All.get(t).size()));
	    	
	    	for (int i=0; i<c3All.get(t).size(); i++) {
	    			temp[i] = c3All.get(t).get(i);
			}
	    	StandardDeviation sd3 = new StandardDeviation();
	    	c3Error.add(sd3.evaluate(temp)/Math.sqrt(c3All.get(t).size()));
	    	Mean mn3 = new Mean();
	    	c3.add(mn3.evaluate(temp, 0, c3All.get(t).size()));	
		}
		
	
	
		imp.setSlice(currentSlice);
		imp.setRoi(roi);
		if (hideImage) imp.getWindow().setVisible(true);
		//imp.getWindow().setSize(dimension);
		System.gc();
		//float[] x = {0.1f, 0.25f, 0.35f, 0.5f, 0.61f,0.7f,0.85f,0.89f,0.95f}; // x-coordinates
	    //float[] y = {2f,5.6f,7.4f,9f,9.4f,8.7f,6.3f,4.5f,1f}; // x-coordinates
	
	    PlotWindow.noGridLines = false; // draw grid lines
	    
	    Plot plot = new Plot("Fucci Fluorescent Plot","Time","Fluorescent intensity");
	
	    plot.setColor(Color.red);
	    //plot.addPoints(t,c3,PlotWindow.X);
	    //plot.addPoints(t,c1,Plot.CIRCLE);
	    plot.addPoints(T,c1,c1Error,Plot.LINE);
	    
	    plot.setColor(Color.green);
	    //plot.addPoints(t,c3,PlotWindow.X);
	    plot.addPoints(T,c2,c2Error,Plot.LINE);
	    // add label
	    plot.setColor(Color.blue);
	    plot.addPoints(T,c3,c3Error,Plot.LINE);
	
	    
		plot.setColor(Color.black);
		//plot.addLegend("1st Channel\t2nd Channel\t3rd Channel\tmitosis");
		plot.addLegend("1st Channel\t2nd Channel\t3rd Channel");
	    plot.show();
	    plot.setLimitsToFit(true);
	
   // double duration = System.currentTimeMillis() -start;
    //println(duration/1000 + " seconds");
    
	}
	
	public static void plotLineageWithImgAndRoiManager(
			ImagePlus imp
			) {
		RoiManager rm = RoiManager.getInstance();
		if (rm == null) return;
		int nROI = rm.getCount();
		
		int currentSlice = imp.getCurrentSlice();
		Roi roi = imp.getRoi();
		

		Roi[] roiArray = rm.getRoisAsArray().clone();
		Arrays.sort(roiArray, new Comparator<Roi>() {
			@Override
			public int compare(Roi r1, Roi r2) {
				return Integer.compare(r1.getTPosition(), r2.getTPosition());
			}
		});
		
		ArrayList<String> lineageSeg = getLineageSegment();
		Map<String, List<Double[]>> map = new HashMap<String, List<Double[]>>();
		
		int numSpot = roiArray.length;
		int tStart = roiArray[0].getTPosition();
		int tEnd = roiArray[roiArray.length-1].getTPosition();
	
		int numC = imp.getNChannels();
		int numZ = imp.getNSlices();
		int numT = imp.getNFrames();
		
		boolean hideImage = nROI>numT;
		if (hideImage) imp.getWindow().setVisible(false);
		
		ArrayList<Double> t = new ArrayList<Double>();
		ArrayList<Double> c1 = new ArrayList<Double>();
		ArrayList<Double> c2 = new ArrayList<Double>();
		ArrayList<Double> c3 = new ArrayList<Double>();
		
		for (int i=0; i<roiArray.length; i++) {
	
			int posT = roiArray[i].getTPosition();
			String name = roiArray[i].getName();
			String lineage = getLineage(name);
			
			boolean pointRoi = roiArray[i].getTypeAsString().equals("Point");
			Roi r = pointRoi ? new Roi(roiArray[i].getXBase(), roiArray[i].getYBase(), 1, 1) : roiArray[i];
			imp.setRoi(r, false);
			
			imp.setPositionWithoutUpdate(1, posT, 1);
			double c1Value = imp.getRawStatistics().mean;
			imp.setPositionWithoutUpdate(2, posT, 1);
			double c2Value = imp.getRawStatistics().mean;
			imp.setPositionWithoutUpdate(3, posT, 1);
			double c3Value = imp.getRawStatistics().mean;
	
			List<Double[]> list = new ArrayList<Double[]>();
			Double[] valueSet = {(double)posT, c1Value, c2Value, c3Value};
			list.add(valueSet);
			if (map.get(lineage)==null) {	// haven't add lineage segment to map
				map.put(lineage, list);
			} else {
				map.get(lineage).addAll(list);
			}
		}
	
		PlotWindow.noGridLines = false; // draw grid lines
	    Plot plot = new Plot("Fucci Fluorescent Plot","Time","Fluorescent intensity");
		ArrayList<String> uniqueElements = getUniqueSegment();
		for (String str : uniqueElements) {
			List<Double[]> list = new ArrayList<Double[]>();
			
			int length = str.length();
			for (int i=0; i<length; i++) {
				String seg = str.substring(0, i+1);
				list.addAll(map.get(seg));
			}
			
			int size = list.size();
			double[] T = new double[size];
			double[] C1 = new double[size];
			double[] C2 = new double[size];
			double[] C3 = new double[size];
			for (int i=0; i<size; i++) {
				T[i] = list.get(i)[0];
				C1[i] = list.get(i)[1];
				C2[i] = list.get(i)[2];
				C3[i] = list.get(i)[3];
			}
			
			plot.setColor(Color.red);
			plot.addPoints(T,C1,Plot.CONNECTED_CIRCLES);
			plot.setColor(Color.green);
			plot.addPoints(T,C2,Plot.CONNECTED_CIRCLES);
			plot.setColor(Color.blue);
			plot.addPoints(T,C3,Plot.CONNECTED_CIRCLES);
		}
		
		
		imp.setSlice(currentSlice);
		imp.setRoi(roi);
		if (hideImage) imp.getWindow().setVisible(true);
	
		plot.setColor(Color.black);
		plot.addLegend("1st Channel\t2nd Channel\t3rd Channel");
	    plot.show();
	    plot.setLimitsToFit(true);
	
		System.gc();
	}
	
	public static String getLineage (String roiName) {
		int idx = roiName.indexOf(",", roiName.indexOf("ID:"));
		if (idx==-1)
			return roiName.substring(roiName.lastIndexOf("-"), roiName.length());
		else
			return roiName.substring(roiName.lastIndexOf("-"), idx);	
	}
	
	public static ArrayList<String> getLineageSegment () {
		RoiManager rm = RoiManager.getInstance();
		if (rm==null) return null;
		int nROI = rm.getCount();
		ArrayList<String> lineageSegments = new ArrayList<String>();
		for (int i=0; i<nROI; i++) {
			String lineage = getLineage(rm.getName(i));
			if (!lineageSegments.contains(lineage))
				lineageSegments.add(lineage);
		}
		return lineageSegments;
	}

	public static ArrayList<String> getUniqueSegment () {
		ArrayList<String> segments = getLineageSegment();
		ArrayList<String> uniqueSegments = new ArrayList<String>();
		Iterator<String> iter = segments.iterator();
		while (iter.hasNext()) {
			String str = iter.next();
			boolean exist = false;
			for (String str2 : uniqueSegments) {
				if (str2.equals(str)) {exist = true; break;}
		   		if (str.contains(str2)) {
		   			uniqueSegments.remove(str2);
		   			break;
		   		}
		   		if (str2.contains(str)) {exist = true; break;}
		   }
		   if (!exist) uniqueSegments.add(str);
		   iter.remove();
		}
		return uniqueSegments;
	}
	
	public static void generateResultTable (ImagePlus sourceImage, String tableName) {
		if (sourceImage==null) return;
		int numC = sourceImage.getNChannels();
		RoiManager rm = RoiManager.getInstance();
		if (rm==null || rm.getCount()==0) return;
		int nROI = rm.getCount();
		
		ResultsTable rt = new ResultsTable();
		for (int i=0; i<nROI; i++) {
			
			Roi roi = rm.getRoi(i);
			if (roi.getTypeAsString().equals("Point")) {
				IJ.error("ROI " + String.valueOf(i+1) + " is a point! Abort table generation.");
				return;
			}
			
			sourceImage.setPositionWithoutUpdate(roi.getCPosition(), roi.getZPosition(), roi.getTPosition());
			sourceImage.setRoi(roi, false);
			
			rt.incrementCounter();
			String roiName = rm.getName(i);
			rt.addValue("ROI", roiName);
			int idx1 = -1; int idx2 = -1; String lineageStr = "NA";
			idx1 = roiName.indexOf("ID:");
			if (idx1!=-1) idx2 = roiName.indexOf("-", idx1);
			if (idx2!=-1) lineageStr = roiName.substring(idx2, roiName.length());
			if (lineageStr.length()==1) lineageStr += "root";
			rt.addValue("lineage", lineageStr);
			//rm.select(sourceImage, i);
			double area = sourceImage.getStatistics().area;
			double xCor = roi.getXBase() + roi.getFloatWidth()/2;
			double yCor = roi.getYBase() + roi.getFloatHeight()/2;
			double tCor = sourceImage.getT();
			rt.addValue("Area", area);
			rt.addValue("X", xCor);
			rt.addValue("Y", yCor);
			rt.addValue("T", tCor);
			double mean = 0.0;
			double stdDev = 0.0;
			for (int c=0; c<numC; c++) {
				sourceImage.setPositionWithoutUpdate(c+1, roi.getZPosition(), roi.getTPosition());
				sourceImage.setRoi(roi, false);
				mean = sourceImage.getStatistics().mean;
				stdDev = sourceImage.getStatistics().stdDev;
				rt.addValue("C"+String.valueOf(c+1)+" mean" , mean);
				rt.addValue("C"+String.valueOf(c+1)+"stdDev", stdDev);
			}
		}
		
		sourceImage.deleteRoi();
		rt.show(tableName + " Data Table");	
	}
	
	@Override
	public void run(String arg) {
		plotWithImgAndRoiManager(IJ.getImage());
	}
	public static void main(String[] args) {

		
		
		
		
		GeneratePlot gp = new GeneratePlot();
		gp.run(null);
		
		
	}
}
